using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.Playwright.NUnit;
using NUnit.Framework;
using System.Text.RegularExpressions;

namespace PlaywrightSample
{
    public class Tests : PageTest
    {

        [Test]
        public async Task FirstStep()
        {
            await Page.GotoAsync("https://playwright.dev");

            await Expect(Page).ToHaveTitleAsync(new Regex("Playwright"));

            var getStarted = Page.Locator("text=Get Started");

            await Expect(getStarted).ToHaveAttributeAsync("href", "/docs/intro");

            await getStarted.ClickAsync();

            await Expect(Page).ToHaveURLAsync(new Regex(".*intro"));
        }
    }
}