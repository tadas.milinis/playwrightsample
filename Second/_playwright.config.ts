const config: PlaywrightTestConfig = {
  //reporter: [['list'], ['html', { outputFolder: 'playwright-report' }]],
  timeout: 60000,
  use: {
    channel: 'chrome',
    headless: true,
    screenshot: 'only-on-failure',
  },

  projects: [
    {
      name: 'Second',
      //testDir: './',
      use: { baseURL: 'https://url.com/' },
    },
  ],
};

export default config;
