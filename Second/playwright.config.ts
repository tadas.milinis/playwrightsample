import { devices, PlaywrightTestConfig } from "@playwright/test";

const config: PlaywrightTestConfig = {
    projects:[{
		testDir: '.',
        name: 'Second'
    }],
    use: {
        // baseURL: 'https://github.com/',
        headless: false,
        viewport: {
            width: 1700,
            height: 1000
        },
        screenshot: 'on',
        video: 'on',
        trace: 'on',
        storageState: './state.json'
    }
}
export default config;
